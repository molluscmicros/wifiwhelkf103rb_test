/* 

wifiwhelk_F103RB_test program for testing the functionality of 
the Mollusc Micros WifiWhelk board using the STM32F103RB.

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
*/

#include "mbed.h"

DigitalOut led(p8);
AnalogIn adc(p9);
Serial ser(PA_9, PA_10);
Serial ser2(p6, p7);
//I2C i2c(pd, pc);
I2CSlave i2c(pd, pc);
//SPI spi(p5, p4, p3);
SPISlave spi(p5, p4, p3, p2);
//DigitalOut cs(p2);
PwmOut pwm(pa);
float v, vcc;

int main() {
    int addr = 0x3b;
    char data[8] = "Hi 012\0";
    //i2c.write(addr, data, 5);
    i2c.address(addr);
    bool i2creceived = true;
    led = 0;
    /*
    while (!i2creceived) {
        led = !led;
        int i = i2c.receive();
        switch (i) {
            case I2CSlave::WriteAddressed:
                i2c.read(data, 8);
                i2creceived = true;
        }
        wait(0.1);
    }
    */
    pwm.period(10.0);
    pwm.write(0.5);
    vcc = 3.3;
    //led = 1;
    ser.format(8, SerialBase::Even, 1);
    ser.baud(57600);
    ser2.baud(9600);
    ser.printf("Hello world!\n");
    ser.printf("CPU = %d Hz\n", SystemCoreClock);
    //cs = 1;
    spi.format(8, 3);
    //spi.frequency(1000000);
    //ser.printf("I2C slave received: %s\n", data);
    spi.reply(0x0);
    int val = 0;
    while (true) {
        if (spi.receive()) {
            val = spi.read();
            led = !led;
            spi.reply((val + 1) & 0xFF);
            //ser2.printf("SPI slave: received: %d\n", val);
        }
        wait(0.01);
    }
    // Below for SPI master test.
    /*
    for (int i = 0; i < 4; i++) {
        cs = 0;
        int val = i * 4 + 2;
        int rep = spi.write(val);
        cs = 1;
        ser.printf("SPI: wrote %d, received %d\n", val, rep);
        wait(0.1);
    }
    */
    led = 1;
    while (true) {
        wait(0.5);
        led = !led;
//        v = adc * vcc;
//        ser2.printf("%0.2f V\n", v);
    }
}
