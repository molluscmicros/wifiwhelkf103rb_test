Mollusc Micros WifiWhelk(F103RB) Test Program
=============================================

This program is designed to test the functionality on the Mollusc Micros WifiWhelk board using the STM32F103RB microcontroller.

Installing this program takes a few steps with the WifiWhelk board is not yet supported in by the official mbed-os, so my fork is needed. The below commands will clone the program, set the WifiWhelk(F103RB) board as the target and compile the program.

````
mbed import https://bitbucket.org:molluscmicros/wifiwhelkf103rb_test
cd wifiwhelkf103rb_test/
rm -rf mbed-os
mbed add https://bitbucket.org:molluscmicros/mbed-os
mbed target MM_WIFIWHELK_F103RB
mbed compile
````

License
-------

Copyright 2016 Nick Kolpin (nick.kolpin@gmail.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
